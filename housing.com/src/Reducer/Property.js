const Property = (state = "", action) => {
  switch (action.type) {
    case "Property": {
      if (action.payload.length === 0) {
        state = "";
        return state;
      }
      state = action.payload;
      return state;
    }
    default:
      return state;
  }
};

export default Property;
