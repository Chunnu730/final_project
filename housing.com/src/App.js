import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./components/Home";
import List from "./components/List";
import Show from "./components/Show";
import Edit from "./components/Edit";
import Signup from "./components/Signup";
import Login from "./components/Login";

export default class App extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <Router>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/list" component={List} />
          <Route exact path="/show" component={Show} />
          <Route exact path="/edit/:id" component={Edit} />
          <Route exact path="/login" component={Login} />
          <Route exact path="/signup" component={Signup} />
        </Switch>
      </Router>
    );
  }
}
