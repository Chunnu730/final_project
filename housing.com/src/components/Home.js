import React from "react";
import Header from "./Header";
import Edge from "./Edge";
import Properties from "./Properties";
import Footer from "./Footer";

export default class Home extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <Header />
        <Edge />
        <Properties />
        <Footer />
      </div>
    );
  }
}
