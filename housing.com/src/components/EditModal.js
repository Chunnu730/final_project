import React from "react";
import { Link } from "react-router-dom";
export default class EditModal extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      display: "block",
    };
  }
  render() {
    if (this.props.flag) {
      return (
        <div className="modal-style" style={this.state}>
          <div>Successfully updated !!!</div>
          <Link to="/show" className=" modal-btn">
            Next
          </Link>
        </div>
      );
    } else {
      return null;
    }
  }
}