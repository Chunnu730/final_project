import React from "react";
import { Link } from "react-router-dom";
import logo from "../images/logo-icon.png";
import ListForm from "./ListForm";

export default class List extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <div className="container-fluid list-nav">
          <div className="row">
            <div className="col-md-2 col-3 m-auto m-md-0 p-md-2">
              <Link to="/">
                <img src={logo} alt="logo" className="img-fluid logo" />
              </Link>
            </div>
            <div className="col-md-10 text-end d-md-block d-none">
              <div className="d-md-flex flex-row justify-content-end mt-1">
                <div className="p-2">
                  <Link className="link" to="/show">Show Property</Link>
                </div>
                <div className="p-2">
                  <Link className="link" to="/list">
                    <button className="btn bg-white px-3 py-0">
                      <span
                        className="btn-text px-1"
                        style={{ color: "blueviolent" }}
                      >
                        List property
                      </span>
                      <span className="w-auto bg-danger free px-1">Free</span>
                    </button>
                  </Link>
                </div>
                <div className="p-2">
                  <Link className="link">Saved</Link>
                </div>
                <div className="p-2">
                  <Link className="link">News</Link>
                </div>
                <div className="p-2">
                  <Link className="link" to={`/login`}>
                    <i class="far fa-user"></i>
                    {localStorage.getItem("user")
                      ? localStorage.getItem("user")
                      : "Login"}
                  </Link>
                </div>
                <div className="p-2">
                  <Link className="link">
                    <i class="fas fa-bars"></i>
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="mt-5">
        <ListForm />
        </div>
      </>
    );
  }
}
