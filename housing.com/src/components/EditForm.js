import React from "react";
import  Property from "../Action/Action";
import { connect } from "react-redux";
import EditModal from "./EditModal";

const mapStateToProps = (props) => {
  return {
    prop: props.property,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    property: (val) => dispatch(Property(val)),
  };
};
class EditForm extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.props.prop.map(element => {
      if(element.id === this.props.id){
        this.state = {
          name: element.name,
          cost: element.cost,
          area: element.addr,
          location:element.location,
          mob: element.mob,
          "name-own": element["name-own"],
          "addr-own": element["addr-own"],
          flag: false,
          id: this.props.id,
        };
      }
    })
  }
  update = () =>{
    const payload = (this.props.prop).map(element => {
         if(element.id === this.props.id){
             element.name = this.state.name;
             element.cost = this.state.cost;
             element.addr = this.state.addr;
             element.location = this.state.location;
             element["name-own"] = this.state["name-own"];
             element.mob = this.state.mob;
             element["addr-own"] = this.state["addr-own"];
             return element;
         }else{
             return element;
         }
     })
     return payload;
 }

  handleOnChange = (event) => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value,
    });
  };
  setData = async (val) => {
    const d = await this.props.property(val);
    console.log(d);
  };
 componentDidMount(){
     console.log(this.props.id);
 }
  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      flag: true,
    });
    this.setData(this.update());
  };
  render() {
    return (
      <div className="container bg-white">
        <EditModal flag={this.state.flag} />
        <div
          className="container mb-4 p-2 border"
        >
          <div className="text-center text-3 py-3">
            <small className="lead">List property details</small>
          </div>
         {(this.props.prop).map(element => {
             if(element.id === this.props.id){
                 return( <form
            class="row g-3 needs-validation"
            novalidate
            onSubmit={this.onSubmit}
          >
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name"
                class="form-control"
                id="validationCustom02"
                placeholder="Name of property"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element.name}
              />
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Cost
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="cost"
                  placeholder="Cost of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element.cost}
                />
              </div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr"
                  placeholder="Address of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element.addr}
                />
              </div>
            </div>
            <div class="col-md-3">
              <label for="validationCustom04" class="form-label">
                Location
              </label>
              <select
                class="form-select"
                id="validationCustom04"
                name="location"
                onChange={this.handleOnChange}
                required
                defaultValue={element.location}
              >
                <option selected disabled value="">
                  --select--
                </option>
                <option>Bangalore</option>
                <option>Chennai</option>
                <option>Pune</option>
                <option>Patna</option>
                <option>Mumbai</option>
              </select>
            </div>
            <div className="text-center text-3 py-3">
            <small className="lead">List Owner details</small>
          </div>
            <div class="col-md-4">
              <label for="validationCustom01" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name-own"
                class="form-control"
                id="validationCustom01"
                placeholder="Enter property name"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element["name-own"]}
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Mobile
              </label>
              <input
                type="number"
                name="mob"
                class="form-control"
                id="validationCustom02"
                placeholder="Mobile number"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element.mob}
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr-own"
                  placeholder="Address"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element["addr-own"]}
                />
              </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-12">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="invalidCheck"
                  required
                />
                <label class="form-check-label" for="invalidCheck">
                  Agree to terms and conditions
                </label>
              </div>
            </div>
            <div class="col-12">
              <button class="btn btn-primary">Update</button>
            </div>
          </form>)
             }else{
                 return null;
             }
         })}
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(EditForm);
