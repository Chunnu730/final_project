import React from "react";
import  Property from "../Action/Action";
import { connect } from "react-redux";
import ReactModal from "./ReactModal";

const mapStateToProps = (props) => {
  return {
    prop: props.property,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    property: (val) => dispatch(Property(val)),
  };
};
class ListForm extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      cost: "",
      area: "",
      location: "",
      mob: "",
      "name-own": "",
      "addr-own": "",
      flag: false,
      id: "",
    };
  }

  uniqueid = () => {
    var idstr = String.fromCharCode(Math.floor(Math.random() * 25 + 65));
    do {
      var ascicode = Math.floor(Math.random() * 42 + 48);
      if (ascicode < 58 || ascicode > 64) {
        idstr += String.fromCharCode(ascicode);
      }
    } while (idstr.length < 32);
    return idstr;
  };
  handleOnChange = (event) => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value,
    });
    if (name === "addr-own") {
      this.setState({
        id: this.uniqueid(),
      });
    }
  };
  setData = async (val) => {
    const d = await this.props.property(val);
    console.log(d);
  };

  data = () => {
    if (!this.props.prop) {
      const payload = [];
      payload.push(this.state);
      return payload;
    } else {
      const payload = this.props.prop;
      payload.push(this.state);
      return payload;
    }
  };
  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      flag: true,
    });
    this.setData(this.data());
  };
  render() {
    return (
      <div className="container bg-white">
        <ReactModal flag={this.state.flag} />
        <div
          className="container mb-4 p-2 border"
        >
          <div className="text-center text-3 py-3">
            <small className="lead">List property details</small>
          </div>
          <form
            class="row g-3 needs-validation"
            novalidate
            onSubmit={this.onSubmit}
          >
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name"
                class="form-control"
                id="validationCustom02"
                placeholder="Name of property"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
              />
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Cost
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="cost"
                  placeholder="Cost of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                />
              </div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr"
                  placeholder="Address of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                />
              </div>
            </div>
            <div class="col-md-3">
              <label for="validationCustom04" class="form-label">
                Location
              </label>
              <select
                class="form-select"
                id="validationCustom04"
                name="location"
                onChange={this.handleOnChange}
                required
              >
                <option selected disabled value="">
                  --select--
                </option>
                <option>Bangalore</option>
                <option>Chennai</option>
                <option>Pune</option>
                <option>Patna</option>
                <option>Mumbai</option>
              </select>
            </div>
            <div className="text-center text-3 py-3">
            <small className="lead">List Owner details</small>
          </div>
            <div class="col-md-4">
              <label for="validationCustom01" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name-own"
                class="form-control"
                id="validationCustom01"
                placeholder="Enter property name"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Mobile
              </label>
              <input
                type="number"
                name="mob"
                class="form-control"
                id="validationCustom02"
                placeholder="Mobile number"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr-own"
                  placeholder="Address"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                />
              </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-12">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="invalidCheck"
                  required
                />
                <label class="form-check-label" for="invalidCheck">
                  Agree to terms and conditions
                </label>
              </div>
            </div>
            <div class="col-12">
              <button class="btn btn-primary">ADD</button>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(ListForm);
