import React from "react";
import Property from "../Action/Action";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import empty from "../images/empty-box.png";
import logo from "../images/logo-icon.png";
import amenities from "../images/amenities.png";
import profile from "../images/profile.png";
import home from "../images/home.png";

const mapStateToProps = (props) => {
  return {
    prop: props.property,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    property: (val) => dispatch(Property(val)),
  };
};
class Show extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
  }
  setData = async (val) => {
    const d = await this.props.property(val);
    console.log(d);
  };

  render() {
    if (!this.props.prop) {
      return (
        <>
          <div className="container-fluid list-nav">
            <div className="row">
              <div className="col-md-2 col-3 m-auto m-md-0 p-md-2">
                <Link to="/">
                  <img src={logo} alt="logo" className="img-fluid logo" />
                </Link>
              </div>
              <div className="col-md-10 text-end d-md-block d-none">
                <div className="d-md-flex flex-row justify-content-end mt-1">
                  <div className="p-2">
                    <Link className="link">Show Property</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link" to="/list">
                      <button className="btn bg-white px-3 py-0">
                        <span
                          className="btn-text px-1"
                          style={{ color: "blueviolent" }}
                        >
                          List property
                        </span>
                        <span className="w-auto bg-danger free px-1">Free</span>
                      </button>
                    </Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">Saved</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">News</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link" to={`/login`}>
                      <i class="far fa-user"></i>
                      {localStorage.getItem("user")
                        ? localStorage.getItem("user")
                        : "Login"}
                    </Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">
                      <i class="fas fa-bars"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container-fluid bg-grey">
            <div className="container bg-white text-center empty-box">
              <div>
                <figure class="figure">
                  <img
                    src={empty}
                    className="figure-img img-fluid rounded p-0 m-0"
                    alt=""
                  />
                  <figcaption
                    class="figure-caption"
                    style={{ marginTop: "-50%" }}
                  >
                    <small className="lead">No properties found</small>
                  </figcaption>
                </figure>
              </div>
            </div>
          </div>
        </>
      );
    } else {
      return (
        <>
          <div className="container-fluid list-nav">
            <div className="row">
              <div className="col-md-2 col-3 m-auto m-md-0 p-md-2">
                <Link to="/">
                  <img src={logo} alt="logo" className="img-fluid logo" />
                </Link>
              </div>
              <div className="col-md-10 text-end d-md-block d-none">
                <div className="d-md-flex flex-row justify-content-end mt-1">
                  <div className="p-2">
                    <Link className="link">Show Property</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link" to="/list">
                      <button className="btn bg-white px-3 py-0">
                        <span
                          className="btn-text px-1"
                          style={{ color: "blueviolent" }}
                        >
                          List property
                        </span>
                        <span className="w-auto bg-danger free px-1">Free</span>
                      </button>
                    </Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">Saved</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">News</Link>
                  </div>
                  <div className="p-2">
                    <Link className="link" to={`/login`}>
                      <i class="far fa-user"></i>
                      {localStorage.getItem("user")
                        ? localStorage.getItem("user")
                        : "Login"}
                    </Link>
                  </div>
                  <div className="p-2">
                    <Link className="link">
                      <i class="fas fa-bars"></i>
                    </Link>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container-fluid bg-white py-5 properties-header">
            <div className="container ">
              <figure>
                <blockquote className="blockquote">
                  <h3>
                    In <strong>Spotlight</strong>
                  </h3>
                </blockquote>
                <figcaption className="mb-3">
                  Find your best place to live with us
                </figcaption>
              </figure>
            </div>
            {this.props.prop.map((element) => (
              <div className="container transform-property">
                <div className="row w-100  border border-shadow">
                  <div className="col px-0">
                    <Link>
                      <img src={home} alt="" className="img-fluid img p-0" />
                    </Link>
                  </div>
                  <div className="col bg-white py-2">
                    <div className="row">
                      <div className="col">
                        <figure>
                          <blockquote className="blockquote">
                            <h5>{element.name}</h5>
                          </blockquote>
                          <figcaption>
                            <div>
                              <small style={{ marginTop: "-17px" }}>
                                {element.area}
                              </small>
                            </div>
                            <div style={{ color: "blueviolet" }}>
                              <strong>
                                <span className="lead">$</span>
                                {element.cost}
                              </strong>
                            </div>
                          </figcaption>
                        </figure>
                      </div>
                      <div className="col">
                        <Link to={`/edit/${element.id}`} className="px-5">
                          <i
                            className="fas fa-edit"
                            style={{ color: "grey" }}
                          ></i>
                        </Link>
                        <button
                          className="border-0"
                          onClick={() => {
                            const data = this.props.prop;
                            const result = data.filter((ele) => {
                              if (ele.id !== element.id) {
                                return true;
                              }
                            });
                            console.log(result);
                            this.setData(result);
                          }}
                        >
                          <span style={{ color: "red" }}>
                            <i className="far fa-trash-alt"></i>
                          </span>
                        </button>
                      </div>
                    </div>
                    <div className="row">
                      <div col>
                        <h5>Amenities</h5>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col">
                        <img src={amenities} alt="" className="img-fluid" />
                      </div>
                    </div>
                  </div>
                  <div className="container">
                    <div className="row">
                      <button
                        className="border-0"
                        onClick={() =>
                          this.setState({
                            toggle: !this.state.toggle,
                          })
                        }
                        style={{ color: "blue" }}
                      >
                        Owner details
                      </button>
                      {this.state.toggle ? (
                        <div style={{ backgroundColor: "#eee" }}>
                          <div class="container py-5">
                            <div class="row">
                              <div class="col-lg-4">
                                <div class="card mb-4">
                                  <div class="card-body text-center">
                                    <img
                                      src={profile}
                                      alt="avatar"
                                      class="rounded-circle img-fluid"
                                      style={{ width: "150px" }}
                                    />
                                    <h5 class="my-3">{element["name-own"]}</h5>
                                    <div class="d-flex justify-content-center mb-2">
                                      <button
                                        type="button"
                                        class="btn btn-outline-primary ms-1"
                                      >
                                        Message
                                      </button>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <div class="col-lg-8">
                                <div class="card mb-4">
                                  <div class="card-body">
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <p class="mb-0">Full Name</p>
                                      </div>
                                      <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                          {element["name-own"]}
                                        </p>
                                      </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <p class="mb-0">Email</p>
                                      </div>
                                      <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                          {"abc@gmail.com"}
                                        </p>
                                      </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <p class="mb-0">Phone</p>
                                      </div>
                                      <div class="col-sm-9">
                                        <p class="text-muted mb-0">2324-3344</p>
                                      </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <p class="mb-0">Mobile</p>
                                      </div>
                                      <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                          {element.mob}
                                        </p>
                                      </div>
                                    </div>
                                    <hr />
                                    <div class="row">
                                      <div class="col-sm-3">
                                        <p class="mb-0">Address</p>
                                      </div>
                                      <div class="col-sm-9">
                                        <p class="text-muted mb-0">
                                          {element["addr-own"]}
                                        </p>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      ) : null}
                    </div>
                    <div></div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </>
      );
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);
