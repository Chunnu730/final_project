import React from "react";
import { connect } from "react-redux";
import Property from "../Action/Action";
import empty from "../images/empty-box.png";
import amenities from "../images/amenities.png";
import profile from "../images/profile.png";
import home from "../images/home.png";
import { Link } from "react-router-dom";
const mapStateTOProps = (props) => {
  return {
    prop: props.property,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    property: (val) => dispatch(Property(val)),
  };
};

class Properties extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
        toggle: "none",
    }
  }
  render() {
    if (!this.props.prop) {
      return (
        <div className="container-fluid bg-grey">
            <div className="container bg-white text-center empty-box">
          <div>
            <figure class="figure">
              <img
                src={empty}
                className="figure-img img-fluid rounded p-0 m-0"
                alt=""
              />
              <figcaption class="figure-caption" style={{marginTop: "-50%"}}>
                <small className="lead">No properties found</small>
              </figcaption>
            </figure>
          </div>
        </div>
        </div>
      );
    } else {
      return<>
             <div
          className="container-fluid bg-white py-5 properties-header"
        >
          <div className="container ">
            <figure>
              <blockquote className="blockquote">
                <h3>
                  In <strong>Spotlight</strong>
                </h3>
              </blockquote>
              <figcaption className="mb-3">
                Find your best place to live with us
              </figcaption>
            </figure>
          </div>
          {(this.props.prop).map((element) => (
            <div className="container transform-property">
              <div className="row w-100  border border-shadow">
                <div className="col px-0">
                  <Link>
                    <img src={home} alt="" className="img-fluid img p-0" />
                  </Link>
                </div>
                <div className="col bg-white py-2">
                  <div className="row">
                    <div className="col">
                      <figure>
                        <blockquote className="blockquote">
                          <h5>{element.name}</h5>
                        </blockquote>
                        <figcaption>
                          <div>
                            <small style={{ marginTop: "-17px" }}>
                              {element.area}
                            </small>
                          </div>
                          <div style={{ color: "blueviolet" }}>
                            <strong>
                              <span className="lead">$</span>
                              {element.cost}
                            </strong>
                          </div>
                        </figcaption>
                      </figure>
                    </div>
                    <div className="col">
                     
                    </div>
                  </div>
                  <div className="row">
                    <div col>
                      <h5>Amenities</h5>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <img src={amenities} alt="" className="img-fluid" />
                    </div>
                  </div>
                </div>
                <div className="container">
                  <div className="row">
                  <button
                      className="border-0"
                        onClick={() =>
                          this.setState({
                            toggle:
                              this.state.toggle === "block" ? "none" : "block",
                          })
                        }
                        style={{color:"blue"}}
                      ><span className="bg-muted">Owner details</span></button>
                    <div style={{ backgroundColor: "#eee" }}>
                      <div class="container py-5" style={{ display: this.state.toggle }}>
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="card mb-4">
                              <div class="card-body text-center">
                                <img
                                  src={profile}
                                  alt="avatar"
                                  class="rounded-circle img-fluid"
                                  style={{ width: "150px" }}
                                />
                                <h5 class="my-3">{element["name-own"]}</h5>
                                <div class="d-flex justify-content-center mb-2">
                                  <button
                                    type="button"
                                    class="btn btn-outline-primary ms-1"
                                  >
                                    Message
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-8">
                            <div class="card mb-4">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Full Name</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {element["name-own"]}
                                    </p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Email</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {"abc@gmail.com"}
                                    </p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Phone</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">2324-3344</p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Mobile</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">{element.mob}</p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Address</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {element["addr-own"]}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>;
    }
  }
}

export default connect(mapStateTOProps, mapDispatchToProps)(Properties);
