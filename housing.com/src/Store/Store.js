import CombinedReducer from "../CombinedReducer/CombinedReducer";
import { createStore } from "redux";

const store = createStore(CombinedReducer);

export default store;