import Property from "../Reducer/Property";
import { combineReducers } from "redux";

const CombinedReducer = combineReducers({
    property: Property,
})

export default CombinedReducer;