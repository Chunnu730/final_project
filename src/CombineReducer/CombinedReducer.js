import Owner from "../Reducer/Owner";
import Location from "../Reducer/Location";
import Property from "../Reducer/Property";
import Prop from "../Reducer/Prop";
import {combineReducers} from "redux";

const CombineReducer = combineReducers({
    owner: Owner,
    location: Location,
    property: Property,
    prop:Prop,
})

export default CombineReducer;