import React from "react";
import Rea from "../images/REAGroup.png";
import { Link } from "react-router-dom";
import logo from "../images/logo-icon.png";
export default class Footer extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <footer>
        <div className="container text-center">
          <h1 style={{ color: "black" }}>
            Part of <img src={Rea} alt="" className="img-fluid img-size" />
          </h1>
        </div>
        <div className="container-fluid main  py-5">
          <div className="row text-center">
            <div className="col">
              <h4 className="py-1">
                <strong>Our brands</strong>
              </h4>
              <p>
                <Link className="footer-link">Real state in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
            </div>
            <div className="col">
              <h4 className="py-1">
                <strong>Proptiger</strong>
              </h4>
              <p>
                <Link className="footer-link">Real state in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
            </div>
            <div className="col">
              <h4 className="py-1">
                <strong> Makaan</strong>
              </h4>
              <p>
                <Link className="footer-link">Real state in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in patna</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in pune</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in bangalore</Link>
              </p>
              <p>
                <Link className="footer-link">Real state in chennai</Link>
              </p>
            </div>
            <div className="col">
              <h4 className="py-1">
                <strong> IRF</strong>
              </h4>
              <p>
                <Link className="footer-link">Flats in mumbai</Link>
              </p>
              <p>
                <Link className="footer-link">Flats in bangalore</Link>
              </p>
              <p>
                <Link className="footer-link">Flats in pune</Link>
              </p>
              <p>
                <Link className="footer-link">Flats in patna</Link>
              </p>
              <p>
                <Link className="footer-link">Flats in chennai</Link>
              </p>
            </div>
          </div>
        </div>

        <div className="container-fluid main  py-5">
          <div className="row text-center">
            <div className="col">
              <h4 className="py-1">
                <img src={logo} alt="logo" className="img-fluid logo" />
              </h4>
              <small>
                <Link className="footer-link">Flats for rent in mumbai</Link>
              </small>
              <small>
                <Link className="footer-link">Flats for rent in chennai</Link>
              </small>
            </div>
            <div className="col">
              <small className="py-1">
                <strong>Company</strong>
              </small>
              <small>
                <Link className="footer-link">Real state in mumbai</Link>
              </small>
              <small>
                <Link className="footer-link">Real state in chennai</Link>
              </small>
            </div>
            <div className="col">
            <small className="py-1">
                <strong>Partners sites</strong>
              </small>
              <small>
                <Link className="footer-link">Real state in mumbai</Link>
              </small>
              <small>
                <Link className="footer-link">Real state in chennai</Link>
              </small>
            </div>
            <div className="col">
            <small className="py-1">
                <strong>Explore</strong>
              </small>
              <small>
                <Link className="footer-link">Real state in mumbai</Link>
              </small>
              <small>
                <Link className="footer-link">Real state in chennai</Link>
              </small>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
