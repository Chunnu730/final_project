import React from "react";
import LoginModal from "./LoginModal";
import icon from "../images/signup.png";
import { Link } from "react-router-dom";
import code from "../images/banner.jpg";
export default class Login extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      email: {
        value: "",
        flag: false,
      },
      password: {
        value: "",
        flag: false,
      },
      modal: false,
    };
  }

  handleOnChange = (event) => {
    if (event.target.name === "email") {
      this.setState({
        email: {
          value: event.target.value,
          flag: true,
        },
        modal: false,
      });
    }

    if (event.target.name === "password") {
      this.setState({
        password: {
          value: event.target.value,
          flag: true,
        },
        modal: false,
      });
    }
  };

  onSubmitForm = (event) => {
    event.preventDefault();
    console.log("submit event");

    if (
      (this.state.email.flag === true) &
      (this.state.password.flag === true) &
      (this.state.email.value === localStorage.getItem("email")) &
      (this.state.password.value === localStorage.getItem("password"))
    ) {
      this.setState({
        modal: true,
      });
    }else{
        alert("invalid userid and password")
    }
  };
  sendData() {
    fetch("https://jsonplaceholder.typicode.com/posts").then((data) => {
      console.log(data);
    });
  }
  render() {
    return (
      <div className="form-container">
        <div className="container-box">
          <div className="container">
            <img src={code} alt="" className="code" />
          </div>
          <div className="container" style={{ marginTop: "-15px" }}>
            <LoginModal flag={this.state.modal} />
            <div className="icon-box">
              <img src={icon} alt="sign icon" className="icon" />
            </div>
            <form onSubmit={this.onSubmitForm}>
              <div className="form-group row ">
                <div className="col">
                  <label>Email</label>
                  <input
                    type="text"
                    className="form-control"
                    required
                    placeholder="Enter email"
                    name="email"
                    defaultValue={this.state.email.value}
                    onChange={this.handleOnChange}
                    autoComplete="off"
                  />
                  <div className="valid-data">{this.state.email.valid}</div>
                  <div className="invalid-data">{this.state.email.inValid}</div>
                </div>
              </div>

              <div className="form-group row ">
                <div className="col">
                  <label>Password</label>
                  <input
                    type="password"
                    className="form-control"
                    required
                    placeholder="Enter password"
                    name="password"
                    defaultValue={this.state.password.value}
                    onChange={this.handleOnChange}
                  />
                  <div className="valid-data">{this.state.password.valid}</div>
                  <div className="invalid-data">
                    {this.state.password.inValid}
                  </div>
                </div>
              </div>
              <div className="form-group row py-3">
                <div className="col">
                  <label>
                    if Don't have account{" "}
                    <Link to="/signup">Register here</Link>
                  </label>
                </div>
              </div>

              <div className="form-group row align-items-center">
                <div class="col-12">
                  <input
                    class="btn btn-primary"
                    type="submit"
                    style={{ width: "100%" }}
                    value={`Login`}
                  />
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}
