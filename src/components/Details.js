import React from "react";
import { Link } from "react-router-dom";
import Head from "./Head";
import Footer from "./Footer";
import amenities from "../images/amenities.png";
import Ownerd from "./Ownerd";
import { Location, Property } from "../Action/Action";
import { connect } from "react-redux";
import axios from "axios";
const mapStateToProps = (props) => {
  return {
    loc: props.location,
    prop: props.property,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    location: (val) => dispatch(Location(val)),
    property: (val) => dispatch(Property(val)),
  };
};

class Details extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
    };
  }

  async componentDidMount() {
    const response = await axios.post(
      "https://api.robostack.ai/external/api/ea54c3b7-3392-43de-9ca0-1b985c3ad98e/properties/",
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-api-key": "7507bdce-65bc-411c-888b-8454988e171b",
          "x-api-secret": "a79d656d-9c39-4f67-accf-b78c2acbd4b4",
        },
      }
    );
    await this.props.property(response.data.results);
    this.setState({
      isDataLoaded: true,
    });
  }
  getValue(nameValue) {
    const locationId = JSON.parse(this.props.loc)["0"].find(
      (element) => this.props.match.params.txt === element.name
    ).id;
    const result = JSON.parse(this.props.prop)["0"].find(
      (element) => element.location_id === locationId
    )[nameValue];
    return result;
  }
  render() {
    //   console.log(this.props.location.value);
    const { isDataLoaded } = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    }
    return (
      <>
        <Head />
        <div
          className="container-fluid bg-white py-5 properties-header"
          style={{ marginTop: "-12%" }}
        >
          <div className="container ">
            <figure>
              <blockquote className="blockquote">
                <h3>
                  In <strong>Spotlight</strong>
                </h3>
              </blockquote>
              <figcaption className="mb-3">
                Find your best place to live with us
              </figcaption>
            </figure>
          </div>
          <div className="container transform-property">
            <div className="row w-100  border border-shadow">
              <div className="col px-0">
                <Link>
                  <img
                    src={this.getValue("image")}
                    alt=""
                    className="img-fluid img p-0"
                  />
                </Link>
              </div>
              <div className="col bg-white py-2">
                <div className="row">
                  <div className="col">
                    <figure>
                      <blockquote className="blockquote">
                        <h5>{this.getValue("name")}</h5>
                      </blockquote>
                      <figcaption>
                        <div>
                          <small style={{ marginTop: "-17px" }}>
                            {this.getValue("address")}
                          </small>
                        </div>
                        <div style={{ color: "blueviolet" }}>
                          <strong>
                            <span className="lead">$</span>
                            {this.getValue("rate")}
                          </strong>
                        </div>
                      </figcaption>
                    </figure>
                  </div>
                  <div className="col"></div>
                </div>
                <div className="row">
                  <div col>
                    <h5>Amenities</h5>
                  </div>
                </div>
                <div className="row">
                  <div className="col">
                    <img src={amenities} alt="" className="img-fluid" />
                  </div>
                </div>
              </div>
              <div className="container">
                <div className="row">
                  <small className="lead">
                    <strong>Owner</strong>
                  </small>
                  <div className="row">
                    <div className="col">
                      <Ownerd id={this.getValue("contact_id")} />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </>
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details);
