import React from "react";
import Header from "./Header";
import Footer from "./Footer";
import Top from "./Top";

export default class Home extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <>
        <Header />
        <Top />
        <Footer />
      </>
    );
  }
}
