import React from "react";
import profile from "../images/profile.png";
import axios from "axios";
import { Owner } from "../Action/Action";
import { connect } from "react-redux";

const mapStateToProps = (props) => {
  return {
    own: props.owner,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    owner: (val) => dispatch(Owner(val)),
  };
};
 class Ownerd extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      isDataLoaded: false,
    };
  }
  async componentDidMount() {
    const response = await axios.post(
      "https://api.robostack.ai/external/api/ea54c3b7-3392-43de-9ca0-1b985c3ad98e/owners/",
      {},
      {
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          "x-api-key": "7507bdce-65bc-411c-888b-8454988e171b",
          "x-api-secret": "a79d656d-9c39-4f67-accf-b78c2acbd4b4",
        },
      }
    );
    await this.props.owner(response.data.results);
    this.setState({
      isDataLoaded: true,
    });
  }

  getValue = (nameValue) => {
    const result = JSON.parse(this.props.own)["0"].find((element) => element.id === this.props.id);
    return result[nameValue];
  };
  render() {
    const { isDataLoaded } = this.state;
    if (!isDataLoaded) {
      return <div className="loader"></div>;
    }
    return (
      <div style={{ backgroundColor: "#eee" }}>
        <div class="container py-5">
          <div class="row">
            <div class="col-lg-4">
              <div class="card mb-4">
                <div class="card-body text-center">
                  <img
                    src={profile}
                    alt="avatar"
                    class="rounded-circle img-fluid"
                    style={{ width: "150px" }}
                  />
                  <h5 class="my-3">
                  {this.getValue("name")}
                  </h5>
                  <div class="d-flex justify-content-center mb-2">
                    <button type="button" class="btn btn-outline-primary ms-1">
                      Message
                    </button>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-8">
              <div class="card mb-4">
                <div class="card-body">
                  <div class="row">
                    <div class="col-sm-3">
                      <p class="mb-0">Full Name</p>
                    </div>
                    <div class="col-sm-9">
                      <p class="text-muted mb-0">{this.getValue("name")}</p>
                    </div>
                  </div>
                  <hr />
                  <div class="row">
                    <div class="col-sm-3">
                      <p class="mb-0">Email</p>
                    </div>
                    <div class="col-sm-9">
                      <p class="text-muted mb-0">{this.getValue( "email")}</p>
                    </div>
                  </div>
                  <hr />
                  <div class="row">
                    <div class="col-sm-3">
                      <p class="mb-0">Phone</p>
                    </div>
                    <div class="col-sm-9">
                      <p class="text-muted mb-0">2324-3344</p>
                    </div>
                  </div>
                  <hr />
                  <div class="row">
                    <div class="col-sm-3">
                      <p class="mb-0">Mobile</p>
                    </div>
                    <div class="col-sm-9">
                      <p class="text-muted mb-0">{this.getValue( "mobile")}</p>
                    </div>
                  </div>
                  <hr />
                  <div class="row">
                    <div class="col-sm-3">
                      <p class="mb-0">Address</p>
                    </div>
                    <div class="col-sm-9">
                      <p class="text-muted mb-0">{this.getValue("address")}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Ownerd)