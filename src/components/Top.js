import React from "react";
import provident from "../images/provident_park.jpeg.webp";
import pri_limited from "../images/private_limited.jpeg.webp";
export default class Top extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="top-project bg-white">
        <div className="container p-2">
          <div className="row">
            <div className="col">
              <h3 style={{ color: "black" }}>
                Top <strong>Projects</strong>
              </h3>
              <h6 style={{ color: "grey" }}>
                Best developers in Bengaluru to explore
              </h6>
            </div>
          </div>
          <div className="row p-3">
            <div className="col  transform-property" style={{borderBottom:"1px solid grey"}}>
              <img
                src={provident}
                alt=""
                className="img-fluid w-100"
                style={{ height: "250px" }}
              />
              <div className="row" >
                <div className="col m-auto">
                  <h5 style={{ color: "black" }} className="px-3">
                    Provident park square
                  </h5>
                  <h6 style={{ color: "grey" }} className="px-3">
                    by provident housing limited
                  </h6>
                  <h6 style={{ color: "black" }} className="p-3">1, 2, 3 BHK Appartments</h6>
                </div>
                <div className="col">
                  <h5 style={{ color: "black" }} className="px-1 text-end">
                    $39.59L - 88.39L
                  </h5>
                  <h6 style={{ color: "grey" }} className="px-3 text-end">
                   price
                  </h6>
                </div>
              </div>
            </div>
            <div className="col  transform-property" style={{borderBottom:"1px solid grey"}}>
              <img
                src={pri_limited}
                alt=""
                className="img-fluid w-100"
                style={{ height: "250px" }}
              />
              <div className="row" >
                <div className="col m-auto">
                  <h5 style={{ color: "black" }} className="px-3">
                    Casa Grand Bolevard
                  </h5>
                  <h6 style={{ color: "grey" }} className="px-3">
                    by Casagrand builder private limited
                  </h6>
                  <h6 style={{ color: "black" }} className="p-3"> 2, 3, 4 BHK Appartments</h6>
                </div>
                <div className="col">
                  <h5 style={{ color: "black" }} className="px-1 text-end">
                    $39.59L - 88.39L
                  </h5>
                  <h6 style={{ color: "grey" }} className="px-3 text-end">
                   price
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
