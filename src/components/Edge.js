import React from "react";

export default class Edge extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div className="section-1 py-3">
        <div className="container-fluid">
          <div className="container p-4">
            <h3 className="mb-4">
              Housing <strong className="mr-3">Edge</strong>{" "}
              <button className="btn bg-white px-3 py-1">Explore all</button>
            </h3>
            <div className="row m-auto">
              <div className="col bg-white p-2 m-1 rounded text-center card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/payRent.4fcf61ad.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Pay Rent</figcaption>
                </figure>
              </div>
              <div className="col bg-white p-2 m-1 rounded text-center card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/rentAgreement.ebdb83e2.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Rent Ageement</figcaption>
                </figure>
              </div>
              <div className="col bg-white p-1 m-1 card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/homeLoan.5909c6c9.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Home Loans</figcaption>
                </figure>
              </div>
              <div className="col bg-white p-1 m-1 card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/homeInteriors.6545d3da.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Home Interiors</figcaption>
                </figure>
              </div>
              <div className="col bg-white p-1 m-1 card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/movers.3f5876da.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Packers & Removers</figcaption>
                </figure>
              </div>
              <div className="col bg-white p-1 m-1 card">
                <figure className="figure text-center">
                  <img
                    src="https://c.housingcdn.com/demand/s/common/assets/solarRooftop.73b9afb5.svg"
                    alt=""
                    className="figure-img img-fluid rounded"
                  />
                  <figcaption>Solar Rooftop</figcaption>
                </figure>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
