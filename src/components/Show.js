import React from "react";
import Head from "./Head";
import home from "../images/home.png";
import { Link } from "react-router-dom";
import amenities from "../images/amenities.png";
import profile from "../images/profile.png";
import { Prop } from "../Action/Action";
import { connect } from "react-redux";

const mapStateToProps = (props) => {
  return {
    pro: props.prop,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    prop: (val) => dispatch(Prop(val)),
  };
};
class Show extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
  }

  setData = async (val) => {
    const d = await this.props.prop(val);
    console.log(d);
  };
  delete(id) {
    const data = JSON.parse(this.props.pro);
    const result = data.filter((element) => {
      if (element.id === id) {
        return true;
      }
    });
    this.setData(result);
  }
  render() {
    return (
      <>
        <Head />
        <div
          className="container-fluid bg-white py-5 properties-header"
          style={{ marginTop: "-12%" }}
        >
          <div className="container ">
            <figure>
              <blockquote className="blockquote">
                <h3>
                  In <strong>Spotlight</strong>
                </h3>
              </blockquote>
              <figcaption className="mb-3">
                Find your best place to live with us
              </figcaption>
            </figure>
          </div>
          {JSON.parse(this.props.pro).map((element) => (
            <div className="container transform-property">
              <div className="row w-100  border border-shadow">
                <div className="col px-0">
                  <Link>
                    <img src={home} alt="" className="img-fluid img p-0" />
                  </Link>
                </div>
                <div className="col bg-white py-2">
                  <div className="row">
                    <div className="col">
                      <figure>
                        <blockquote className="blockquote">
                          <h5>{element.name}</h5>
                        </blockquote>
                        <figcaption>
                          <div>
                            <small style={{ marginTop: "-17px" }}>
                              {element.area}
                            </small>
                          </div>
                          <div style={{ color: "blueviolet" }}>
                            <strong>
                              <span className="lead">$</span>
                              {element.cost}
                            </strong>
                          </div>
                        </figcaption>
                      </figure>
                    </div>
                    <div className="col">
                      <Link to={`/edit/${element.id}`} className="px-5">
                        Edit
                      </Link>
                      <button
                        onClick={() => {
                          const data = JSON.parse(this.props.pro);
                          const result = data.filter((ele) => {
                            if (ele.id !== element.id) {
                              return true;
                            }
                          });
                          console.log(result);
                          this.setData(result);
                        }}
                      >
                        <span style={{ color: "red" }}>Delete</span>
                      </button>
                    </div>
                  </div>
                  <div className="row">
                    <div col>
                      <h5>Amenities</h5>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col">
                      <img src={amenities} alt="" className="img-fluid" />
                    </div>
                  </div>
                </div>
                <div className="container">
                  <div className="row">
                    <small className="lead">
                      <strong>Owner</strong>
                    </small>
                    <div style={{ backgroundColor: "#eee" }}>
                      <div class="container py-5">
                        <div class="row">
                          <div class="col-lg-4">
                            <div class="card mb-4">
                              <div class="card-body text-center">
                                <img
                                  src={profile}
                                  alt="avatar"
                                  class="rounded-circle img-fluid"
                                  style={{ width: "150px" }}
                                />
                                <h5 class="my-3">{element["name-own"]}</h5>
                                <div class="d-flex justify-content-center mb-2">
                                  <button
                                    type="button"
                                    class="btn btn-outline-primary ms-1"
                                  >
                                    Message
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-lg-8">
                            <div class="card mb-4">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Full Name</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {element["name-own"]}
                                    </p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Email</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {"abc@gmail.com"}
                                    </p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Phone</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">2324-3344</p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Mobile</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">{element.mob}</p>
                                  </div>
                                </div>
                                <hr />
                                <div class="row">
                                  <div class="col-sm-3">
                                    <p class="mb-0">Address</p>
                                  </div>
                                  <div class="col-sm-9">
                                    <p class="text-muted mb-0">
                                      {element["addr-own"]}
                                    </p>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div></div>
                </div>
              </div>
            </div>
          ))}
        </div>
      </>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Show);
