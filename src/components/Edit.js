import React from "react";
import { Prop } from "../Action/Action";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import logo from "../images/logo-icon.png";
const mapStateToProps = (props) => {
  return {
    pro: props.prop,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    prop: (val) => dispatch(Prop(val)),
  };
};
class Edit extends React.Component {
  // eslint-disable-next-line
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      cost: "",
      area: "",
      location: "",
      mob: "",
      "name-own": "",
      "addr-own": "",
      flag: false,
      pgDisplay: "none",
      id: this.props.match.params.id,
    };
  }

  uniqueid = () => {
    var idstr = String.fromCharCode(Math.floor(Math.random() * 25 + 65));
    do {
      var ascicode = Math.floor(Math.random() * 42 + 48);
      if (ascicode < 58 || ascicode > 64) {
        idstr += String.fromCharCode(ascicode);
      }
    } while (idstr.length < 32);
    return idstr;
  };
  handleOnChange = (event) => {
    const name = event.target.name;
    this.setState({
      [name]: event.target.value,
    });
  };
  setData = async (val) => {
    const d = await this.props.prop(val);
    console.log(d);
  };

  update = () =>{
    const payload =  JSON.parse(this.props.pro).map(element => {
         if(element.id === this.props.match.params.id){
             element.name = this.state.name;
             element.cost = this.state.cost;
             element.addr = this.state.addr;
             element.location = this.state.location;
             element["name-own"] = this.state["name-own"];
             element.mob = this.state.mob;
             element["addr-own"] = this.state["addr-own"];
             return element;
         }else{
             return element;
         }
     })
     return payload;
 }
  onSubmit = (event) => {
    event.preventDefault();
    this.setState({
      flag: true,
    });
    this.setData(this.update());
    alert("ok");
  };
  render() {
    return (<>
    
    <div className="prop">
          <header>
            <div className="container-fluid">
              <div className="row">
                <div className="col-md-2 col-3 m-auto m-md-0 p-md-2">
                  <img src={logo} alt="logo" className="img-fluid logo" />
                </div>
                <div className="col-md-10 text-end d-md-block d-none">
                  <div className="d-md-flex flex-row justify-content-end mt-1">
                    <div className="p-2">
                      <Link className="link">Download App</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link" to={`/show`}>Show Property</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">
                        <button className="btn bg-white px-3 py-0">
                          <span
                            className="btn-text px-1"
                            style={{ color: "blue" }}
                          >
                            List property
                          </span>
                          <span className="w-auto bg-danger free px-1">
                            Free
                          </span>
                        </button>
                      </Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">Saved</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">News</Link>
                    </div>
                    <div className="p-2">
                      <Link className="link" to={`/signup`}>
                        <i class="far fa-user"></i>Login
                      </Link>
                    </div>
                    <div className="p-2">
                      <Link className="link">
                        <i class="fas fa-bars"></i>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container text-center mt-0 p-lg-3">
              <div className="row m-auto">
                <div className="col">
                  <p className="display-6 text-white">
                    Properties to sell in <strong>Bangaluru</strong>
                  </p>
                  <h5>
                    <span className="w-auto p-lg-5 p-md-1 mb-lg-4 text-white">
                      Yahan Search Khatam Karo
                    </span>
                  </h5>
                </div>
              </div>
            </div>
            <div className="container text-center p-3 mt-lg-3">
              <div className="row">
                <div className="col">
                  <div className="">
                    <select
                      name="location"
                      id="location"
                      defaultValue={this.state.value}
                      className="px-2 py-3 round w-50"
                      onChange={this.handleOnChange}
                    >
                      <option value="Bangalore">Bangalore</option>
                      <option value="Mumbai">Mumbai</option>
                      <option value="Patna">Patna</option>
                      <option value="Pune">Pune</option>
                      <option value="Chennai">Chennai</option>
                    </select>
                  </div>
                </div>
              </div>
            </div>
          </header>
        </div>

      <div className="container bg-white mb-5" style={{ marginTop: "-5%" }}>
        {/* <ReactModal flag={this.state.flag} /> */}
        <div
          className="container mb-4 p-2 border"
        >
        <h1>ok</h1>
          <div className="text-center text-3 py-3">
            <select
              class="form-select"
              id="validationCustom04"
              name="propType"
              className="col-4 py-2"
              required
              defaultValue={this.state.type}
              onChange={this.handleOnChange}
            >
              <option>Property</option>
              <option>PG</option>
            </select>
          </div>
          {JSON.parse(this.props.pro).map(element => {
            if(element.id === this.props.match.params.id){
              return(<form
            class="row g-3 needs-validation"
            novalidate
            onSubmit={this.onSubmit}
          >
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name"
                class="form-control"
                id="validationCustom02"
                placeholder="Name of property"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element.name}
              />
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Cost
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="cost"
                  placeholder="Cost of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element.cost}
                />
              </div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr"
                  placeholder="Address of peroperty"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element.addr}
                />
              </div>
            </div>
            <div class="col-md-3">
              <label for="validationCustom04" class="form-label">
                Location
              </label>
              <select
                class="form-select"
                id="validationCustom04"
                name="location"
                onChange={this.handleOnChange}
                required
                defaultValue={element.location}
              >
                <option selected disabled value="">
                  --select--
                </option>
                <option>Bangalore</option>
                <option>Chennai</option>
                <option>Pune</option>
                <option>Patna</option>
                <option>Mumbai</option>
              </select>
            </div>
            <h4 className="text-center">Owner details</h4>
            <div class="col-md-4">
              <label for="validationCustom01" class="form-label">
                Name
              </label>
              <input
                type="text"
                name="name-own"
                class="form-control"
                id="validationCustom01"
                placeholder="Enter property name"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element["name-own"]}
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustom02" class="form-label">
                Mobile
              </label>
              <input
                type="number"
                name="mob"
                class="form-control"
                id="validationCustom02"
                placeholder="Mobile number"
                onChange={this.handleOnChange}
                required
                autoComplete="off"
                defaultValue={element["mob"]}
              />
              <div class="valid-feedback">Looks good!</div>
            </div>
            <div class="col-md-4">
              <label for="validationCustomUsername" class="form-label">
                Address
              </label>
              <div class="input-group has-validation">
                <input
                  type="text"
                  class="form-control"
                  name="addr-own"
                  placeholder="Address"
                  id="validationCustomUsername"
                  aria-describedby="inputGroupPrepend"
                  onChange={this.handleOnChange}
                  required
                  autoComplete="off"
                  defaultValue={element["addr-own"]}
                />
              </div>
            </div>
            <div class="col-md-3"></div>
            <div class="col-12">
              <div class="form-check">
                <input
                  class="form-check-input"
                  type="checkbox"
                  value=""
                  id="invalidCheck"
                  required
                />
                <label class="form-check-label" for="invalidCheck">
                  Agree to terms and conditions
                </label>
              </div>
            </div>
            <div class="col-12">
              <button class="btn btn-primary">ADD</button>
            </div>
          </form>)
            }else{
              return null;
            }
          })}
        </div>
        </div>
        </>
    );
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Edit);
