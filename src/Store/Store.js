import CombineReducer from "../CombineReducer/CombinedReducer";
import {createStore} from "redux";

const store = createStore(CombineReducer);
export default store;