const Property = (state=JSON.stringify([]), action) => {
    switch (action.type) {
      case "Property": {
        state=JSON.parse(state);
        state.push(action.payload);
        return JSON.stringify(state);
      }
  
      default:
        return state;
    }
  };
  export default Property;