const Owner = (state=JSON.stringify([]), action) => {
    switch (action.type) {
      case "Owner": {
        state=JSON.parse(state);
        state.push(action.payload);
        return JSON.stringify(state);
      }
  
      default:
        return state;
    }
  };
  export default Owner;