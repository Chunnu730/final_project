const Prop = (state="", action) => {
    switch (action.type) {
      case "Prop": {
        state = action.payload;
        return JSON.stringify(state);
      }
  
      default:
        return state;
    }
  };
  export default Prop;