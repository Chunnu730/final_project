const Owner = (val) => {
    return({
        type: "Owner",
        payload: val
    })
}

const Location = (val) => {
    return {
        type: "Location",
        payload: val
    }
}

const Property = (val) => {
    return {
        type: "Property",
        payload: val
    }
}

const Prop = (val) => {
    return {
        type: "Prop",
        payload: val
    }
}
export {Owner, Location, Property, Prop}